# Pomodoro

Yeah, the name is bad and generic. I'll fix that eventually.

## Install/Usage

### With Git

```
git clone https://gitlab.com/j3rn/pomodoro
cd pomodoro
./pomodoro # Start the timer
```

### Without Git

1. Download the file: https://gitlab.com/J3RN/pomodoro/raw/master/pomodoro
2. Mark it as executable:
    ```
	chmod +x pomodoro
	```
3. Run it:
   ```
   ./pomodoro
   ```
